# Sandbox API Usage 
## Resources

* /api/status
* /api/log/import
* /api/log/reset
* /api/upload
* /api/import
* /api/reset

## Status values
* running
* importing
* resetting
* down
* failed
* failed_import
* failed_reset

## Usage
### Check status
<code>curl -H "Authorization: Bearer secret-token" -k https://FQDN:portnumber/api/status</code>


### Import log
<code>curl -H "Authorization: Bearer secret-token" -k https://FQDN:portnumber/api/log/import</code>

### Reset log
<code>curl -H "Authorization: Bearer secret-token" -k https://FQDN:portnumber/api/log/reset</code>


### File upload
<code>curl --form file=@europdx.tgz -H "Authorization: Bearer secret-token" -X POST -k https://FQDN:portnumber/api/upload</code>

### Init data import
<code>curl -X POST -H "Authorization: Bearer secret-token" -k https://FQDN:portnumber/api/import</code>

### Init sendbox reset to default
<code>curl -X POST -H "Authorization: Bearer secret-token" -k https://FQDN:portnumber/api/reset</code>

<br>

*Note:* ```-k``` *is there to disable cURL SSL certificate verification*

## Example
<code>curl -H "Authorization: Bearer secret-token" -k https://sandbox1.edirex.ics.muni.cz:5001/api/status</code>
