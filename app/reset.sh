#!/bin/bash
set -x

whoami

HOME_DIR=/home/pdxuser
COMPOSE_DIR=$HOME_DIR/sandbox-docker
CBIO_DIR=$HOME_DIR/cbioportal-docker-compose

#TEST_STUDY=https://gitlab.ics.muni.cz/europdx/sandbox/data/-/raw/master/test-study.zip?inline=false

rm -r /tmp/import


# Just info
docker ps
docker images

# stop cbio docker stack
cd $CBIO_DIR 
docker-compose down
docker volume prune -f


# stop dataportal stack
cd $COMPOSE_DIR
docker-compose down
docker volume prune -f


# Download and unarchive test-study.zip - import-user-data
#wget $TEST_STUDY -O /tmp/test-study.zip
#rm -r $COMPOSE_DIR/import-user-data/*
#unzip /tmp/test-study.zip -d $COMPOSE_DIR/import-user-data
#rm /tmp/test-study.zip

# Populate import-data
# TODO change source
#cp $HOME_DIR/data4reset/import-data/*.json $COMPOSE_DIR/import-data

# Populate neo4j-data
#TODO change source
cp $HOME_DIR/data4reset/neo4j-data/graph.tgz $COMPOSE_DIR/neo4j-data


# Run indexer?

# Start the dataportal stack
cd $COMPOSE_DIR
docker-compose up -d


# Start the cbio stack
cd $CBIO_DIR
./reset_cbioportal.sh


# Wait untill stack is up

attempt_counter=0
max_attempts=5

until $(curl --insecure --output /dev/null --silent --head --fail https://localhost); do
    if [ ${attempt_counter} -eq ${max_attempts} ];then
      echo "Max attempts reached"
      exit 1
    fi

    printf '.'
    attempt_counter=$(($attempt_counter+1))
    sleep 5
done

# DS - init DataHub
sleep 30
#cd ~/datahub-docker/api/
#./init_datahub.sh
docker exec  datahub bash -c "sed -i 's/localhost/db/g' config.py"
docker exec datahub ./init_datahub_from_container.sh

#Change status to running
