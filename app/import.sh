#!/bin/bash

whoami

set -x
HOME_DIR=/home/pdxuser
COMPOSE_DIR=$HOME_DIR/sandbox-docker
DOCUMENTS=$HOME_DIR/Documents
IMPORT_FOLDER=/tmp/import

# Make sure emty ~/Documents directory exists
mkdir -p $DOCUMENTS
rm -r $DOCUMENTS/pdx.graphdb/*
mkdir -p $DOCUMENTS/pdx.graphdb

# Extract seed db to ~/Documents/pdx.graphdb
tar -zxvf $HOME_DIR/data4reset/neo4j-data/graph.tgz --directory $DOCUMENTS/pdx.graphdb


# Launch loader
cd $HOME_DIR/dataportal-sourcecode
java -jar indexer/target/indexer-1.0.0.jar load --group=EurOPDX --data-dir=$IMPORT_FOLDER/europdx


# Tar created N4J DB files
cd $HOME_DIR/Documents/pdx.graphdb
tar -czvf ../graph.tgz *


#Move graph.tgz to $COMPOSE_DIR/neo4j-data
mv -f ../graph.tgz  $COMPOSE_DIR/neo4j-data 

#Restart Sandbox stack
cd $COMPOSE_DIR
docker-compose down
docker volume rm sandbox-docker_neodata 
docker-compose up -d


# DS - init DataHub
sleep 30
#cd ~/datahub-docker/api/
#./init_datahub.sh
docker exec  datahub bash -c "sed -i 's/localhost/db/g' config.py"
docker exec datahub ./init_datahub_from_container.sh


cd $HOME_DIR/sandbox-api/app
./load-custom-study.sh
