#!/bin/bash

#export ID=`curl -X POST -H "Content-Type: application/json" -k https://localhost:5000/api/v1/tmplists/NKIX | jq '.tmplistid'`
export ID=`curl -X POST -H "Content-Type: application/json" -k https://localhost:5000/api/v1/tmplists/all | jq '.tmplistid'`

if [ $ID = "null" ];
then
    echo "There are no pdxmodels for NKIX study"
else
    echo $ID
    rm -r _studies/*

    # create files for cBioPortal
    /home/pdxuser/data-providing/cbio_client.py 

    # move the files to cBioPortal folder
    rm -r /home/pdxuser/cbioportal-docker-compose/study/*
    cp -r _studies/* /home/pdxuser/cbioportal-docker-compose/study/

    # run cBioPortal's importer
    cd /home/pdxuser/cbioportal-docker-compose/
    ./import_studies.sh
fi
