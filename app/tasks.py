import time
import logging
import os
import subprocess
import tarfile

from app import app
from config import Config
from app.tools import init_folder, set_status_value, get_status_value


#def example(seconds):
#    print('Starting task')
#    for i in range(seconds):
#        print(i)
#        time.sleep(1)
#    print('Task completed')


def import_data():
    logging.basicConfig(level=logging.DEBUG, handlers=[logging.FileHandler("log/tasks.log"), logging.StreamHandler()])
    logging.info("Starting tgz file extraction.")
    tgz_file_full_path = os.path.join(app.config['UPLOAD_FOLDER'], app.config['UPLOAD_FILE'])
    if os.path.isfile(tgz_file_full_path):
        tar = tarfile.open(tgz_file_full_path)
        tar.extractall(path=app.config['IMPORT_FOLDER'])
        tar.close()
        logging.info("Tgz file extracted.")
    else:
        logging.info("Tgz file not available.")
    # TODO launch shell script
    stdout_file = open(app.config['IMPORT_STDOUT'], 'w')
    stderr_file = open(app.config['IMPORT_STDERR'], 'w')

    # TODO launch shell script
    proc = subprocess.run("./app/import.sh", stdout=stdout_file, stderr=stderr_file)

    logging.info('Return code: ' + str(proc.returncode))

    if proc.returncode == 0:
        set_status_value("running")
        logging.info("Setting status to running.")
    else:
        set_status_value("failed_import")
        logging.info("Setting status to failed.")


def reset_sandbox():
    logging.basicConfig(level=logging.DEBUG, handlers=[logging.FileHandler("log/tasks.log"), logging.StreamHandler()])
    logging.info("Starting sandbox reset process.")

    stdout_file = open(app.config['RESET_STDOUT'], 'w')
    stderr_file = open(app.config['RESET_STDERR'], 'w')

    # TODO launch shell script
    proc = subprocess.run("./app/reset.sh", stdout=stdout_file, stderr=stderr_file)

    logging.info("Sandbox reset process finished.")
    logging.info('Reset bash return code: ' + str(proc.returncode))

    if proc.returncode == 0:
        set_status_value("running")
        logging.info("Setting status to running.")
    else:
        set_status_value("failed_reset")
        logging.info("Setting status to failed.")
